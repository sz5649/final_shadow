from django.shortcuts import render
import StringIO
from django.http import HttpResponse
from django.template import Context, loader
from models import Temperature
import datetime
from django.db.models import Max
import json
import time
import readadc

# Create your views here.
def graph(request):
        # temperature sensor middle pin connected channel 0 of mcp3008
        sensor_pin = 0
        readadc.initialize()
	sensor_data = readadc.readadc(sensor_pin,
                                  readadc.PINS.SPICLK,
                                  readadc.PINS.SPIMOSI,
                                  readadc.PINS.SPIMISO,
                                  readadc.PINS.SPICS)

        millivolts = sensor_data * (3300.0 / 1024.0)
        temp = ((millivolts - 100.0) / 10.0) - 40.0
        

	# Temperature.objects.all().delete()
	Temperature.objects.create(temp_C = temp, count = 0)
	#temp_text = Temperature.objects.get(count=0)
	alltmp = Temperature.objects.all()
	temp_text = alltmp[len(alltmp)-1]
	timenow = datetime.datetime.now()
	t = loader.get_template('tempSensor/index.html')
	c = Context({
            'temp_text': temp_text,
            'time': timenow
        })

	# temp2 = Temperature.objects.create(temp_C = 20, count = 1)
	# temp_text2 = Temperature.objects.get(count=1)
	return HttpResponse(t.render(c))
	
