#import plotly.plotly as py
import json
import time
import readadc
import datetime
from models import Temperature


# temperature sensor middle pin connected channel 0 of mcp3008
sensor_pin = 0
readadc.initialize()

# clear the data base
Temperature.objects.all.delete()

#the main sensor reading and plotting loop
cnt = 0
while cnt < 1000:
    sensor_data = readadc.readadc(sensor_pin,
                                  readadc.PINS.SPICLK,
                                  readadc.PINS.SPIMOSI,
                                  readadc.PINS.SPIMISO,
                                  readadc.PINS.SPICS)

    millivolts = sensor_data * (3300.0 / 1024.0)
#    print sensor_data
#    print millivolts  
  # 10 mv per degree
    temp_C = ((millivolts - 100.0) / 10.0) - 40.0
    # convert celsius to fahrenheit
    temp_F = (temp_C * 9.0 / 5.0) + 32
    # remove decimal point from millivolts
    millivolts = "%d" % millivolts
    # show only one decimal place for temprature and voltage readings
    temp_C = "%.2f" % temp_C
    temp_F = "%.2f" % temp_F

    # write the data to plotly
    print temp_C
    # insert data into database
    Temperature.objects.create(temp_C, count=cnt)
    

    # delay between stream posts
    time.sleep(0.25)
    cnt += 1
